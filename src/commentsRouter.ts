import express,{  Request, Response} from "express"
import {  getCommentsByUSerId, postComment, deleteComment, updateComment
} from "./dao"
const router = express.Router()




//GET /comments/:userID Lists all comments by the user defined by the userId parameter

router.get("/:id", async (req: Request, res: Response) => {
    const post = await (getCommentsByUSerId(parseInt(req.params.id))) 
    return res.send(post.rows)
})


// POST /comments Adds a new comment

router.post("/", async (req:Request, res:Response) => {
    const {user_id, post_id, content} = req.body
    const comment = await (postComment(user_id,post_id,content))
    return res.send(comment.rows[0])
})

// PUT /comments/:id modify content

router.put("/:id",async (req: Request, res: Response) => {
    const {  id } = req.params
    const content = req.body.content
    await updateComment(content,parseInt(id))
    return res.send(req.body)
} )

// DELETE /comments/:id deletes comment by id

router.delete("/:id", async (req: Request, res: Response) => {
    const {  id } = req.params
    await deleteComment(parseInt(id))
    return res.send({id: parseInt(id), message: "deleted"})
})

export default router