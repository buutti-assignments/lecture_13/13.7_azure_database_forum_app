import server from "./server"

const { PORT } = process.env

server.listen(PORT, () => {
    return console.log('Forum API listening to port', PORT)
})






