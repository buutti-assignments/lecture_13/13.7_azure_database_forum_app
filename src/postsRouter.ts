import express,{  Request, Response} from "express"
import {  getAllPosts, getPostsAndCommentsByPostId,addPost, deletePost, updatePost
} from "./dao"
const router = express.Router()


// GET /posts Lists all post id's, poster id's and titles


router.get("/", async (req: Request, res: Response) => {
    const results = await getAllPosts()
    const posts = results.rows.map((post) => {return {id: post.post_id, user_id: post.user_id,title: post.title}})
    return res.send(results.rows)
})

// GET /posts/:id Sends detailed information of the post, including all the comments associated with the post.

router.get("/:id", async (req: Request, res: Response) => {
    const post = await (getPostsAndCommentsByPostId(parseInt(req.params.id))) 
    return res.send(post.rows[0])
})

// POST /posts Adds a new post

router.post("/", async (req: Request, res: Response) => {
    const {  title,content,user_id} = req.body
    const result = await addPost(title, content, user_id)
    return res.send(result.rows[0])
})

// PUT /posts/:id update title and content

router.put("/:id",async (req: Request, res: Response) => {
    const {  id } = req.params
    await updatePost(parseInt(id), req.body)
    return res.send(await (await getPostsAndCommentsByPostId(parseInt(id))).rows[0])
})


//DELETE /posts/:id deletes post by id
router.delete("/:id", async (req: Request, res: Response) => {
    const {  id } = req.params
    await deletePost(parseInt(id))
    return res.send({id: parseInt(id), message: "deleted"})
})

export default router