import server from '../src/server'
import request from 'supertest'
import { jest } from '@jest/globals'
import { pool } from '../src/db'

const initializeMockPool = (mockResponse: any) => {
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null,
        }
    })
}


// Comments tests

describe('Testing  /posts', () => {
  const mockResponse = {
    rows: [
      {
        post_id: 1,
        user_id: 1,
        content: "buu",
        title: "title1",
        comment_date: new Date().toString(),
      },
      {
        post_id: 2,
        user_id: 1,
        content: "bau",
        title: "title 2",
        comment_date: new Date().toString(),
      },
      
    ],
  };
  beforeAll(() => {
      initializeMockPool(mockResponse)
  })
  afterAll(() => {
      jest.clearAllMocks()
  })


  it('get /posts/ returns all posts ', async () => {
      
      const response = await request(server).get('/posts/')
      expect(response.body).toStrictEqual(mockResponse.rows)
  })
 
  it('get /posts/:id returns post and comments associated by post_id', async () => {
      
    const response = await request(server).get('/posts/1')
    expect(response.body).toStrictEqual(mockResponse.rows[0])
})


it('post /posts/ makes new post and returns it ', async () => {
      
  const response = await request(server).post('/posts/')
  .send({user_id: 1,
    content: "buu",
    title: "title 1"})
  expect(response.body).toStrictEqual(mockResponse.rows[0])
})

it('PUT /posts/ makes new post and returns it ', async () => {
      
  const response = await request(server).put('/posts/1')
  .send({
    content: "buu",
    title: "title 1"})
  expect(response.body).toStrictEqual(mockResponse.rows[0])
})


it('DELETE /posts/:id deletes post by id ', async () => {
      
  const response = await request(server).delete('/posts/1')
  expect(response.body).toStrictEqual({id: 1, message: "deleted"})
})


})